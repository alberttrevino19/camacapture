import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";

import { Nav, Navbar, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import './components/Navbar/Navbar.css';

import AboutPage from "./components/Views/AboutPage";
import ContactPage from "./components/Views/ContactPage";
import PricingPage from "./components/Views/PricingPage";
import PhotosPage from "./components/Views/Photos Pages/PhotosPage";
import GradPage from "./components/Views/Photos Pages/GradPage";
import WeddingPage from "./components/Views/Photos Pages/WeddingPage";
import FamilyPage from "./components/Views/Photos Pages/FamilyPage";
import MiscPage from "./components/Views/Photos Pages/MiscPage";
//import HomePage from "./components/Views/HomePage";



ReactDOM.render(
  <React.StrictMode>
    <Router>
      <div className="App">
        <Navbar className="NavbarItems" collapseOnSelect expand="lg" bg="light" variant="light">
          <Container fluid>
            <Navbar.Toggle aria-controls="navbarScroll" />
            <Navbar.Collapse id="navbarScroll">

              <Nav className="nav-menu-left" navbarScroll>
                <Nav.Link as={Link} to="/about" className="nav-links">About</Nav.Link>
                <Nav.Link as={Link} to="/photos" className="nav-links">Photos</Nav.Link>
              </Nav>

            </Navbar.Collapse>

            <Navbar.Brand as={Link} to="/">
              <img
                src="https://bl6pap003files.storage.live.com/y4mwfOEDde6GZDUSnKYYIJ176wJwfb3j6ZXstUDG6gnWAbYn75YQD3IBJ_NzgG2td33TFD5PnfSo0XUX5c8yfiFbr4Mmc4MFlAn-0JI63XoEOSrE_-CdS_3FriuMiOrsTJ5bHryyfHV7YxVExuRRxqpGlixkbFLqG9tB3dAt1yBRGc9G7VsMWyhhGpG9ex9a6SfOLETGNK9WOJzwXEPKTvvNw/dark.png?psid=1&width=895&height=895&cropMode=center"
                alt="logo"
                width="100"
                height="100"
                className="navbar-logo"
              /></Navbar.Brand>

            <Navbar.Collapse id="navbarScroll">
              <Nav className="nav-menu-right" navbarScroll>
                <Nav.Link as={Link} to="/pricing" className="nav-links">Pricing</Nav.Link>
                <Nav.Link as={Link} to="/contact" className="nav-links">Contact</Nav.Link>
              </Nav>
            </Navbar.Collapse>

          </Container>
        </Navbar>
        <Routes>
          <Route path="/" element={<App />} />
          <Route path="/about" element={<AboutPage />} />
          <Route path="/photos" element={<PhotosPage />} />
          <Route path="/pricing" element={<PricingPage />} />
          <Route path="/contact" element={<ContactPage />} />
          <Route path="/grad" element={<GradPage />} />
          <Route path="/wedding" element={<WeddingPage />} />
          <Route path="/family" element={<FamilyPage />} />
          <Route path="/misc" element={<MiscPage />} />
        </Routes>
      </div>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

