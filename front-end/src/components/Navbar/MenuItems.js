export const MenuItems = [
    {
        title: 'About',
        url: '#',
        cName: 'nav-links'
    },
    {
        title: 'Photos',
        url: '#',
        cName: 'nav-links'
    },
    {
        title: 'Pricing',
        url: '#',
        cName: 'nav-links'
    },
    {
        title: 'Contact',
        url: '#',
        cName: 'nav-links'
    }
]