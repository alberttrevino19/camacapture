import React, { Component } from "react";
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {Link} from "react-router-dom";
//import NavDropdown from 'react-bootstrap/NavDropdown';
//import { MenuItems } from "./MenuItems";
import './Navbar.css';

class CamaNavbar extends Component {
    state = { clicked: false }
    handleClick = () => {
        this.setState({ clicked: !this.state.clicked })
    }

    render() {
        /*return (
            <nav className="NavbarItems">
                <h1 className="navbar-logo"><img src="https://blufiles.storage.live.com/y4m8vc7I0-ZuzPDS0ilr51VMRbBLIA15pfzyRyiD5SlKYAdUpW6rbrmlwkiSBjzoknpPi6QlXBmKWNmKS30HIVmTp56B52kMeR592qrljGV43JAtL94iTnPNkB-OzXPcw4KJ51GsjatBmbBoCFgfH7jI4ah6OMjAL8irqpPnDlyWSY9Hq-J0EwblgtrwkeZn_59NtMHPX851zXGfXokPat4KA/dark.png?psid=1&width=897&height=897&cropMode=center" alt = "Logo" width = "100" height = "100"></img></h1>
                <div className="menu-icon" onClick={this.handleClick}>
                    <i className = {this.state.clicked ? 'fas fa-times' : 'fas fa-bars'}></i>
                </div>
                <ul className={this.state.clicked ? 'nav-menu active' : 'nav-menu'}>
                    {MenuItems.map((item, index) => {
                        return(
                            <li key={index}>
                                <a className={item.cName} href={item.url}>
                                {item.title}
                                </a>
                            </li>
                        )
                    })}

                </ul>

            </nav>
        ) */
        return (
            
                <Navbar className="NavbarItems" collapseOnSelect expand="lg" bg="light" variant="light">
                    <Container fluid>
                        <Navbar.Brand href="#home">
                            <img
                                src="https://bl6pap003files.storage.live.com/y4mwfOEDde6GZDUSnKYYIJ176wJwfb3j6ZXstUDG6gnWAbYn75YQD3IBJ_NzgG2td33TFD5PnfSo0XUX5c8yfiFbr4Mmc4MFlAn-0JI63XoEOSrE_-CdS_3FriuMiOrsTJ5bHryyfHV7YxVExuRRxqpGlixkbFLqG9tB3dAt1yBRGc9G7VsMWyhhGpG9ex9a6SfOLETGNK9WOJzwXEPKTvvNw/dark.png?psid=1&width=895&height=895&cropMode=center"
                                alt="logo"
                                width="100"
                                height="100"
                                className="navbar-logo"
                            /></Navbar.Brand>
                        <Navbar.Brand href="#home2">
                            CamaCapture Photography
                        </Navbar.Brand>
                        <Navbar.Toggle aria-controls="navbarScroll" />
                        <Navbar.Collapse id="navbarScroll">
                            <Nav className="me-auto my-2 my-lg-0"
                                style={{ maxHeight: '300px' }}
                                navbarScroll>
                            </Nav>
                            <Nav className="nav-menu">
                                <Nav.Link as={Link} to="/about" className="nav-links">About</Nav.Link>
                                <Nav.Link as={Link} to="/photos" className="nav-links">Photos</Nav.Link>
                                <Nav.Link as={Link} to="/pricing" className="nav-links">Pricing</Nav.Link>
                                <Nav.Link as={Link} to="/contact" className="nav-links">Contact</Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
            
        )
    }
}

export default CamaNavbar