import React, { Component } from "react";
import "./PricingPage.css"

class PricingPage extends Component {
    render() {
        return (
            <div className="pricing-info">

                <div className="column">
                    <h2>Package 1</h2>
                    <li>1 hour shoot</li>
                    <li>1 location</li>
                    <li>1 outfit</li>
                    <li>~15-20 edited photos delivered digitally</li>
                    <img className="bottom-img" alt='another grad pic'
                        src='https://bn3pap090files.storage.live.com/y4mUEOOz7jKcNnlxGNCGOFh_Wl9VT4Do2GxQJzuNbOA0PaETBwnDJSB11nZRFY5LtYYqPDVfIcpTPE5gI8H7sGH9nzeLi47SY-vxm5apr6HgNwaQS04b-jRZ1ipDZ9Ez_66x49dlSHbcR17pFAqexjQvYhccPTHcIy6R-pIinNGviMqsWodDMvcjj9abRrBXcjJHz4ilO3tzgHbDoN-NS5DxQ/DSC_2008.jpg?psid=1&width=5484&height=3656'></img>
                </div>

                <div className="column">
                    <img alt='grad pic'
                        src='https://snz04pap001files.storage.live.com/y4mKXIV-S1S2IOlHXu3mbqlAQSx83fihmU9XarUM5cuzGDmKQ5Yo0YEZu8N1xFB4Hy1VkRBqGL9Rxii7buTCeAbB8EzQmaZe_2d-ygJlVtQ7hc6OZaEMk9ZV77pUZL4OEHAyw6N4l6pZFwSoy2J0b8keFG0ZtyuH-J2K6eD12Tbr34dlbMFC66UTW-0H5vfxUqBJh8G1jNiJRyh_-mapaU1Xw/DSC_0217.jpg?psid=1&width=5754&height=3836'></img>
                    <h2>Package 2</h2>
                    <li>2 hour shoot</li>
                    <li>1-2 locations</li>
                    <li>2 outfits</li>
                    <li>~35-40 edited photos delivered digitally</li>
                </div>

                <div className="bottom-txt">
                    All package prices are negotiable
                </div> 
            </div>
        );
    }

}

export default PricingPage;