import React, { Component } from "react";
import "./AboutPage.css"
import { Link } from "react-router-dom";
class AboutPage extends Component {
    render() {
        return (
            <div className="about">
                <div className="column1">
                    <img className="headshot" alt='headshot' src='https://snz04pap001files.storage.live.com/y4mAnxnDU2i88xlVa1_ymXXyqq0sExjnJFaRQEqYjhaw35U-42hN0gM3YrlX2Ef2TIk9zpF1_S8K0A7tvVRsWI8Bsz1cDPf3_OF1mJxqwUvFCFqsC-zuCADfvHX5fEgk6LdofzLmrUdG1gZqRAclSyi6LAFhd7WAi_7VKcmzBFeEnofDQRLJgybEsVqaz0kDBnhD-7c_hQ7J-LrnnKSC54kLA/20200309_153613470_iOS.jpg?psid=1&width=610&height=915'></img>
                </div>
                <div className="column1">
                    <h2>THE ONE BEHIND THE CAMERA </h2>
                    <p className="text-about">Hey! The name’s Gerardo. Others call me Gerry. Whatever you prefer. I think it’s important to know who exactly is responsible for capturing your memories, so I’ll do my best to summarize who I am.</p>

                    <p className="text-about">My full name is Gerardo Camarillo II. I have a wife and 2 beautiful children. A humble kingdom of 4, but they eat the equivalent of a much bigger one. A proud nerd, I enjoy video games, high fantasy and sci-fi novels, manga, movies, lots of music, and I love talking about all of them. Please bring them up whenever we meet!</p>

                    <p className="text-about">As for why I decided to shoot photos for a living, there’s a few reasons. I very much enjoy every aspect of the creative process. I get excited thinking about a shoot the next day, my heart pounds getting all the equipment that I need ready, I itch to start sifting through a day’s shoot and begin editing them. On top of that,
                        I love making others happy. I love seeing their reactions to their memories being frozen in time in a visual token that lasts forever. Being a dad comes with discovering emotions you never knew you could experience, and I believe those emotions play a role in the work that I do. Graduations, weddings, engagements, families, couples,
                        even simple professional headshots; all of these have a story to tell. Branches and branches of personal tales that are impossible to fully know. My job is to do my best to understand your story, your moments, and pour those feelings into the photos I shoot for you.
                        Let’s talk! </p>

                    <Link to='/Contact'>Contact Me!</Link>

                </div>

            </div>
        );
    }

}

export default AboutPage;