import React, { Component } from "react";
import { Slide } from "react-slideshow-image";
import {
    CCard, CCardImage, CCardBody,
    CCardTitle, CCardText, CCardFooter, CRow, CCol
} from '@coreui/react';
import "./HomePage.css";
import "react-slideshow-image/dist/styles.css";
import { Navbar, Container } from "react-bootstrap";
import { Link } from "react-router-dom";



class HomePage extends Component {

    constructor() {
        super();
        this.slideRed = React.createRef();
        this.state = {
            current: 0
        };
    }

    render() {
        const properties = {
            duration: 2500,
            autoplay: true,
            transitionDuration: 750,
            arrows: true,
            infinite: true,
            easing: "ease",
            //indicators: (i) => <div className="indicator">{i+1}</div>
        };
        const slideImages = [
            "https://snz04pap001files.storage.live.com/y4mvI8L6ca_05sl_aqNb1gWqRoXbDTHS-9jHsOlWEhlOgL1YiITLeAHv2JRs2oCc6qZqBwil4aysU_jjWbcZ_XavFCxfn0cN81CH4_ixL8OJfD0veaKVHrdQL5eCgWedPI88NrRyXWQ3vUKPr8i_5gcRQakcdsXKewWP9AkqKmc3uR307pqaGhsMmfaKDDFAFF4J1TH6GcZdC1z69Xnv8-OrQ/DSC_0621.jpg?psid=1&width=5754&height=3836",
            "https://snz04pap001files.storage.live.com/y4mrumPoHY8lYOzFryVDeAouF7vkxfibimb5nv0tVrnbwLhKOkP8_LhqjzlHHW6zPZUuwr_WWRUYU3nroK_mVSk581WslkvJ3obFu5S4RlTRNdTtpTEJf28RpVNctIXq6nwTnCDCqOT43Bzv9dM0oHdn1I_4rwnJ6kAeLkP1tWta9BhTDAONQ733R35uCXIbV2hyJ3eqq3mizdxrZS3WAenzg/DSC_0075.jpg?psid=1&width=5755&height=3836",
            "https://bn1304files.storage.live.com/y4mNglkdDJSGU3o8T17s0xOzYUWcVABM27UfHFRFlCGXf9R21TiiscT1Z1lEobBqVUbjVwHEuQoSdtKPmjuCs_4pw4tDDXUaj5OulmzPTml_ZtIMPtKdzlAjvsZdpLh-UrinWlAXUwt6BMuinoGT5i7L_zYKu5FYLurMpLyD-OlOnm7GC_e1H7yC8QBxGxqmMr4v0wyfh8AG-HvGkk2bcKcZA/DSC_2557.jpg?psid=1&width=5676&height=3784",
            "https://bn1301files.storage.live.com/y4magJqp5wN_yfEphcMK2G-dcf_qj8KqO53MInY8rdxp9EgQSqdD4-_TQ6NQGUWZUR9Vlthd7U68zyNV9oZ0Fz_35xuBUDhqrOVTZWDLDaCZd8OsamyF4QziGL7JXdHC_PZcXCndg_A1d5VjiIxZOr3T-eAMESeH3Xvoi1bhS72VzOQrHzqAR86XRYaw6PTf2-7G7nxa03HW5recZElOHG4Zg/DSC_2590.jpg?psid=1&width=5676&height=3784",
            "https://bn1ap000files.storage.live.com/y4m-F-oxLnvzHcJUbl4Ou5eXB7QFmlRlv0FCoIntvHRDJlKqFcqrHHwfEaGZlCgggDQVj4obFAef1tIMTbYs6L2caXooqtMbjufefL2vd99yqaq1RkLgPeXCkrSQzfuVkFaDvquR51M87IEkF9s8ZRNKKX-lg0I3outQZpUqIMc6irHKSO3Tjz9Sj3bMla3juSLTZObBrme1-HFmxygr341bA/DSC_2040.jpg?psid=1&width=5676&height=3784",
            "https://bnz05pap001files.storage.live.com/y4moxYqIShKhzAkLTKZXYmjpOqkO_2PVfK9Z2nermRTLCgeuw2JRYi40NEEAJ7ZIxrIcJ2wLEYlVeFQcUZ4MteDZMEngim0ovEbCF6qJyqjxWpEg2mLkfnb9BZjfybTIs-aUGpDRBKn9YnzwOS6uFNFZc0Z64-YxyYONWNjH-1u6UC1HicaYwPAbwKnZprVS589h2X23kb0bnQs_4XEW7vfCg/DSC_0061.jpg?psid=1&width=5676&height=3784",
            "https://bnz05pap001files.storage.live.com/y4mxDih06Jx9-U4mT0ThQ-z7qEXKq8Sx6GLGVs8NUPZoNNl2oWDnshLpJ_PVmZOMROBW3jfh7N9fhYmIRFUCOxTlozGIppT-sfCYNM0IUmYGpDp5RF5tUnskuQvxw3cFpCYmFsly-DRzVyMksR7VRc4Um5s5MpzSIGQ8untXAetQwmrUGpNgV-I2Bss5Vj0UIgUhb66ksG4ZQcvk2Q7WB0mFQ/DSC_0102.jpg?psid=1&width=5676&height=3784",
            "https://bn1files.storage.live.com/y4mxlV30pjvKJlS958hES1JQflsGtVBjYZx0TXpfHCkerKzvp7DTkdsx8XjvS4wio41CqpNIzM-_uvToh0m4RFzcesGFQvlTTr2BQWkLjdyN8GQc6Vo98-ClH3l8UDawO3E_fCk3C6Xe5A_3qcMqXb56H1F-1-cOD_rH2cDxWwv_lgM_u4R-1RkhSCBfr-kGHpI2J67LcbLSZHkqR0GYQ2paA/DSC_0542.jpg?psid=1&width=5676&height=3784",
            "https://bnz05pap002files.storage.live.com/y4mM5fQ_cGtnUyM-6FLs5BXTmYEY7hEs21x73_7TVCwSM13SxmKSoJvLQKmAwtIfwxAu4OcXhMLt55irlh0I4qp73hAwUzylzoIpXVF-vJflhwVk5dlOgbPdbYOA1EdhHA9QbLhEISs7hhQXl2vszt9yoklEZWXgLqDCKQVdWm94Y-ivalBqUGRGYR-bLsZnEJNt_h2ra6mmvIQcc1qbehVGw/DSC_0045.jpg?psid=1&width=4344&height=2896",
            "https://blufiles.storage.live.com/y4mifhq-nr36xShmRQNFzlmV-mApqGKGZ3j1CeQKGtAX0NpaM-4kRo0s-mMVw4qHOn2NItsoeFvicuCW_tsDkBVNOj87MaBTbmxPcmdXVZbNrvwq9K3Ng0w1Dlsl0z1tNfzhntq5OyxQ3f-CBUB9xueQhSn52QDg-XAJDUV0Rtn_wLrbttiPETLe6k0udEeOCscj3-YK3Ub8JHS9BRm1AZ5yg/DSC_0897-2.jpg?psid=1&width=3000&height=2000",
            "https://bl3301files.storage.live.com/y4mDb7zjGRLu_c3fRHTT5Zu59JWEnbav6BcU3fLkaqq4pK6m0el1rvo8m6IRTEJRRtvrtRSmqBU8N19FABSaK26bm8iXcS4PXQxkny0B0i2BXome7xmiME15QLK63vwwKWigEB6uTEd1JN5LcmBN40N1bGEnnvw4ATYT-9GLx4kgc2Yp86-hQp0FX5KlGfX3x1iIL30AxW27z5b2P76UsZPvw/DSC_0132-Recovered.jpg?psid=1&width=5676&height=3784",
            "https://bl6pap004files.storage.live.com/y4maiWswr8WkDGaHCZ8q9498XtowZyEjKb2w-J12G5isJ6Wb2dhJTrZy8OQGFH004JLS2RTG4O9W3li6ZGERUC7NOX1QqAx-Ycq8BRNfyjDw_hFZSXmPY2i4s6RdSNwodFwbiJ2Go5pP9kRfMtqwHNo903IYfJB5jPueYMrpFslfj0BsTzZCWRMGqFFNNYomO9O0vI8oQfsQKGrejxF87QlLg/DSC_0553-2-Recovered.jpg?psid=1&width=5676&height=3784",
            "https://bl3302files.storage.live.com/y4m_hRdLYdDqmMAKlHwKrrCd5Zj1rRIQL1KHrC1k4cLOM5TK4IT38B37WpRYAwNnzXbBfNASlxVbkGA0l6ljPdId9YXFcQekCEZDLm_cZr9gZswv3E1Bz9OWYxsIvK9mtxS0Rxm7Slrr3o7yuKQeNn7ijinX3wVIB5CBG6pUNg9TRfnhM6QGeHG1juQZrY0qYqg28jVj6jaHjMa_JqJ5foNzA/DSC_0909.jpg?psid=1&width=5676&height=3784",
            "https://blufiles.storage.live.com/y4mG4d8NLswAjVTg2_2sd7mLSaogLA3tU2ieLfilXswqtxXH-Jkp2FywA4EptYxyOo1N9s1W1W3jPo6UERTf8_QcDGQ_JOuLnYyn3s3WMES8DqpbLHS7c_AOWtKmJ3ZriSNmSUZ9Df6i0Punf1JHvNTiQs9X3cGjbQv0NIpV-nPfngRVnyub0BjPhfPU5gP_GDtPjRzqFT4qszY7BBIjmNBGQ/DSC_0914.jpg?psid=1&width=5676&height=3784",
            "https://snz04pap001files.storage.live.com/y4mI27F2f4PoUXkfK9W6Gd2dT-PgRGdtW4MVTlx-VTHrAoy5ohG-0QcQAL7OZtCMT2fwK9hi3T0fFL4OrO78a5QAxvTPAWpSRkCng0cfHsIKLPs-JsTnP31zYIibUfQbkEFORrzwJZAWw-NpsomUhKbwEO0gUB555pO0io9Ez-QAPxMAhOU5iftn0i-CtqDNKJFFpZqGyFNV9a_NUuXnNoC0g/DSC_1179%20%281%29.jpg?psid=1&width=5754&height=3836",
            "https://bl3301files.storage.live.com/y4mH3_YDFikvAb2UQi9NGvLvFaugzqfK-fj4rCDao57HB3SFM0sGs3XrUJRLkG_mJsKwsraUHK9mc7yE2wn5SpE2_Dk2B_cVu1AoYI6noe9ajP1IBUj8FEkrGFNw2t_vZcUQKoSJp31mu9DGUkXPwO6vx-XjBSYpcYfdtRtrQSuqkhsKEByIvGla9TzDbVg-j4aF-2KL6p-t2_mf62xQvGRMg/DSC_0672.jpg?psid=1&width=5676&height=3784",
            "https://bl3302files.storage.live.com/y4mqX7oZ2Hqwmwdp_rIdOpS8b_616wyUZiVy5oOo1gAmpp4-8WaeedVERoZL1If7sD668-__4tyU60jtleVmVjDZPqquOiOrbJblng96wkZBY9LttD_LuPa9NOd9kkdGv7KH0j7qt5eTKntQk9NcQnZvoglc5sqb9kQxbMqgo_X-X6fqB_T6fv9YY7zM6GA--ffnOMkY70sp8TJ7_JWVXYM5Q/DSC_1190.jpg?psid=1&width=5676&height=3784",
            "https://bl3302files.storage.live.com/y4mk2Z7F1PVgVWXSw3vZg7FPuvaPpuFNskNB3Het03laZD3P3XGj30gKMISRsPKkssWVIMRVduufAcnm6ofeYatmu7pfaslramcbffH7e9E_89R2Vk13wt0qZW2O1KHYwC2MZJbCcVp9ispWP0b42RfgPjALkmzj_S42TX4plV2_r3OXTg61greqti48ncVB6Qj2WmIRuqD_Gmi4Hyq95Ecbw/IMG_6834.jpg?psid=1&width=5676&height=3784",
            "https://bl3301files.storage.live.com/y4my0LWs1xk3GoktvxNhz-PQ8a3xU8JToBqrriOoO9QdN5sxWH4wn4Ghz5bdSLBx-dzDTOSqLsRpPzrojdQbmHI9MicHX8ZP0oT5fG-exnL8L3RD4_RYyCy8XO8MkVC_PavYQvvJAk8WwFdvrfYTrnqZRIttXmuDFs01UM7TfacwkKBI6BKQ6CAQfgPL7ybPCHd4ITXYUaLKlSl68MXEWu9SQ/IMG_6954-2.jpg?psid=1&width=5676&height=3784",
            "https://bl3301files.storage.live.com/y4mWCGPMns8v35enDDigkVnkHarCGn5UKz2QLmOeCvl_D_n-S0i3ZilKr3wWnwLYPcAv5SnZtxueHp849YR1dL7V8ucO_muR4XkQGlABTXj4ePTfcVM2vItSHz7876-RQ8j6pg1XkA0l3niAbiEgEITqVcmlTQI69DlzqJ_sRhp8RWYXFv8w7D316lBnRNQIuxWqb5jur-uf7QXW7PPf7XLpg/IMG_7992.jpg?psid=1&width=5676&height=3784",
            "https://snz04pap001files.storage.live.com/y4moLF-pWvW8JrfQtMwtRsfRTEoVA_oTuOfOuXYs20t-45giaazcBlRjCXe5T8Vh4ob0CgODBJd-Qj16hqnHZaHNzhrWrg9SiAftFLFNOXrCpA5VBeT3GnBi1htu9nNP5nNrAX-494j9G7CeEqOjpuvbgWdbf0ZuvWpliNAx8nb79secJRCy9bdoUl8Hpaul7o6fQy74hdZQe3KCzQ91Zxw9A/IMG_8840.jpg?psid=1&width=5754&height=3836",
            "https://bl6pap004files.storage.live.com/y4mah1dUsGJzo-tvinKvsEHn6C42kEFdJM045PsQuDv39VDTvtLOjGfUwONMoje0NgVWweOaDxAqmrL1Uhc4AEQb6BNeMQJFpvXKfT4d2yoPyXNJcNdmoTV58pqSSqWRTvIc-VvoHIP2h2yOXLnQD4GQO_Hz8iZJbIvczEgCbhbarog7FV0_3CgCN8kS4X0GmmBIpIKE-XBDrC6xZtLDSoraA/IMG_9651.jpg?psid=1&width=5676&height=3784",
            "https://blufiles.storage.live.com/y4m6izMH1VAqDzDoTPoIC5vPahkAtxfTfPzNAbl-MpQeCBSUbH9QWJvXpHlc8eNkn87oNXN-qzM1tkMwxxUevN-y2HOtsIu4cvI7RN4l3PtKptOlvjW3iQzPWlfVbAqYeycs5nwtVw8YqKCIAwrizxUWQ4R6g84dsy_wxBqNgNwp7U70nIHJvNnar3cgCHeBfebAnEkakxI0GhGVxbX28CfGw/IMG_9652.jpg?psid=1&width=5676&height=3784",
            "https://blufiles.storage.live.com/y4mOZ7KWAjwOehT50_HElBXLFwRbaBCu185rb_kbZSm6-Vxgi-2xrVEKP4sAjdLlS8VKDbuQcwCZ4so-Aao__eXkW62yqxdemBKYALxsI2LhVnAwesWULicVva23E-cTKQO3xWPx6H7sKecY4DDgSozKTrGPANfxs-KYTmJ5FM06CDCyI7nypkc3ys3jcfKBDxQ7X3VzUQnmS4dKVvji2ibIw/IMG_9703.jpg?psid=1&width=4423&height=2949",
            "https://bl3301files.storage.live.com/y4mI-WOPm7oeiHRXF25ozUUxC4O_WH5ufV-SXBNFNb_rWnpfiC_pOtyyJNPEz7ihR208p1RDVFQvPxjyzA2yJiCnjdcmXUN98nxp1UNcUGJQfQXGDmJn56les9Xy3Y3ME-z_80ONGNlwsI0S-V2jytq5-OO-azerGHk1Xetx_ujNfZMCsLmZ1tgUQrpw_nEbnNbjtoj-9vxD2nq_VNys9fr6Q/IMG_0631.jpg?psid=1&width=4423&height=2949",
            "https://bl6pap003files.storage.live.com/y4mVYyTuomZPqYhakQI02xFu-jdEiqpMB6bjrWLN0PxJ2dJMnmL4tlRsLnzbssZdCAdNqcrGDtUge4PAgTxkOFEko0efbmcZukViVkg5JP4SuwqNZOfgxAqf3yV_GWKXUZMe4flHZ01fm2Vsc7-kLBlxWa-WZ_PZNq8RgC3N7ghgH156E0L2ZA86UKlQ8UpxNcOW_KPkKotwdIrtjdWJXOYaQ/IMG_2184.jpg?psid=1&width=4423&height=2949",
            "https://blufiles.storage.live.com/y4mLJlWDngfdwe9r_Wnbgjbjnb-rmtl5aQosPAz1OpBoLgSOP7Tk8gfDx_cenY5CiqccPvgs-nuEY9TtASz76nA6VRhR5XrwoxAUw3PDze3CLf_OaIcRZ1AWb2-Os9VP_eueFvWUPBeKIPSZSxHDV3ZNXsQVYv3xKGgj6_pvB7PliuaJkFzRVBKtNBSlJdFcI1pLpuSWc4XWfDPYTeuw4iG1A/IMG_7331.jpg?psid=1&width=5676&height=3784",
            "https://snz04pap001files.storage.live.com/y4m86Ds3nGzJ61tA-1UomapDPwfzCSQiNsyFOZ6RS-AVUjfjLTitdehSQx2sTn3-QwYENT3HnH9F0g2wezJhjUXwZJJp0b7OCO8n1T_HNbrjt0S1jYr4231pmzVr7ZXcQJfxS_m9oofrxoRn3agkVypN4D6k66fbq9mcd5Nyz03kwqIlhgKIA-CSZ9230hfdk-LA0V0dlMuimAgKOp3uUj0Cg/IMG_0029.jpg?psid=1&width=5676&height=3784",
            "https://snz04pap001files.storage.live.com/y4mBjvl03qTEYMWoKw2rOilOsH8NvGtOg7R4J_n9K7SMhdAyKwoQiuVf3ha8zzaL4Ky1cPWv2Hh4B0VMWZPkoB_Ju-i5Rdvzepeuint53KvwtejrzpoLoAOD1qGf70Y9Dl0AK7pbMYgZVcOGcHa3L2KlpuOpgs8n20itEFPQJBa24YVKbEFsN0IqfEf9CbadP39dvKoLOGJ6e4GxvGJBFus9A/IMG_0029%20%281%29.jpg?psid=1&width=5676&height=3784"
        ];
        return (
            <div className="HomePage">
                <div className="slide-container">
                    <Slide ref={this.slideRef} {...properties}>
                        {slideImages.map((each, index) => (
                            <div key={index} className="each-slide">
                                <img src={each} alt="sample" />
                            </div>
                        ))}
                    </Slide>
                    <div className="card-container-home">
                        <CRow xs={{ cols: 1, gutter: 4 }} md={{ cols: 4 }}>
                            <CCol xs>
                                <Link to={'/grad'} className="text-link">
                                    <CCard className="card-home">
                                        <CCardImage orientation="top" src="https://snz04pap001files.storage.live.com/y4mNmODxPGwzwj3DPtbvTfbvP4bTGsordBqxytuG3nLEI7odbNI-BOV5ZVKVhn2NlMoa8fhayZJN1dWe3AmcAKCnDwJQJStHqzgTM7YmTq_FL8bnI9scemePnyh2WYKsJmhgELSMJKwV--Gpo1XRa4Kd5GcpjhsSXjnb17Yy37PhipTRGdmGIGWpkbxwx-KHsVXuVczPehtnWk0917z4q-W5w/IMG_0140.jpg?psid=1&width=1419&height=946" />
                                        
                                        <CCardBody>
                                        <CCardTitle>Graduation Pictures</CCardTitle>
                                            <CCardText>
                                                Feel confident while having your personality shine through the photos.
                                            </CCardText>
                                        </CCardBody>
                                        <CCardFooter>
                                            <small className="text-medium-emphasis">Click for more...</small>
                                        </CCardFooter>
                                    </CCard>
                                </Link>

                            </CCol>

                            <CCol xs>
                                <Link to={'/wedding'} className="text-link">
                                    <CCard className="card-home">
                                        <CCardImage orientation="top" src="https://snz04pap001files.storage.live.com/y4mtTNy9gLPTwlobTF3nRMAAexT-9z_00hTnrPHk0bCo0ox6T--_5jOHN2ikYLLn094xOeOoHclgwD2U1d_jUiU6QCCXmBLMXwl_6OA27Vttbj8rxBfxl9LyESlOiJ6HzEp3bEtBOkm_W1bgB9XzhBP1fcrKE57p6tDk9jsqdaX30vOb905kuVlmeNVkGWgHdu5zQYwErofJYynB6XTSDbqWw/IMG_0015.jpg?psid=1&width=1365&height=946" />
                                        
                                        <CCardBody>
                                        <CCardTitle>Weddings and Engagements</CCardTitle>
                                            <CCardText>
                                                Capture that special moment with that special person.
                                            </CCardText>
                                        </CCardBody>
                                        <CCardFooter>
                                            <small className="text-medium-emphasis">Click for more...</small>
                                        </CCardFooter>
                                    </CCard>
                                </Link>

                            </CCol>

                            <CCol xs>
                                <Link to={'/family'} className="text-link">
                                    <CCard className="card-home">
                                        <CCardImage orientation="top" src="https://bl3302files.storage.live.com/y4mWq2LDGnl23htrkX6m-Ucpvs-4NtQOUIaDQReNm2LQNa6TPxKY6AbGpre6HejUFkW1cPywF0S2naMf4VmWW7lpnPESrEWiXX9gWAmZ71aUfOc0OD9hAf497RQAnK9-7ZJRJtbAaAzWudo2vaLH5nKOVz4B0800yCaYCTr3fEwxypT6NuvXyKQqF1Ned_7as9kqCMC1dsfI7w0tWmukKRyMw/IMG_6880-2.jpg?psid=1&width=1419&height=946" />
                                        <CCardBody>
                                        <CCardTitle>Couples and Families</CCardTitle>
                                            <CCardText>
                                                Pictures are meant to be fun and capture any moment with such ease.
                                            </CCardText>
                                        </CCardBody>
                                        <CCardFooter>
                                            <small className="text-medium-emphasis">Click for more...</small>
                                        </CCardFooter>
                                    </CCard>
                                </Link>

                            </CCol>

                            <CCol xs>
                                <Link to={'/misc'} className="text-link">
                                    <CCard className="card-home">
                                        <CCardImage orientation="top" src="https://bl3301files.storage.live.com/y4mYEAmhnkzYKg-aGWynHEz1AhRrCbKSmL2lbgdWffcRF0IRy9m1LuY9USXSVv4OMCvw2IZmD_UamtkwfaSVL5ltoPqJUPHQNI2WeHiV4DOLFW9FOIJXPZ2Od21NLAYamqMjIljR91PPs0ufHdSD_xZlNbKPw1PxKguImqeD6Rj1w6v2cjUVZvxL4QmxFF8-w2On18vY3WXrnBM9rNdBbjdXw/DSC_0069_1.jpg?psid=1&width=1419&height=946" />
                                        <CCardBody>
                                        <CCardTitle>Portraits & More</CCardTitle>
                                            <CCardText>
                                                Need a new portrait to hang on an empty wall or for that next job interview? You
                                                can be the next piece of art.
                                            </CCardText>
                                        </CCardBody>
                                        <CCardFooter>
                                            <small className="text-medium-emphasis">Click for more...</small>
                                        </CCardFooter>
                                    </CCard>
                                </Link>

                            </CCol>
                        </CRow>

                        <Navbar className="bottom-bar" collapseOnSelect expand="lg" bg="light" variant="light">
                        <Container fluid>
                            <Navbar.Brand className='fb-link' target="_blank" href="https://www.facebook.com/camacapture">
                                <img
                                    src="https://upload.wikimedia.org/wikipedia/commons/0/0c/Facebook%2BIcon%2BBlack.png"
                                    alt="logo"
                                    width="50"
                                    height="50"
                                    className="bottom-pics"
                                /></Navbar.Brand>
                            <Navbar.Brand target="_blank" href="https://www.instagram.com/camacapture/">
                                <img
                                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Black_Instagram_icon.svg/1200px-Black_Instagram_icon.svg.png"
                                    alt="logo"
                                    width="50"
                                    height="50"
                                    className="bottom-pics"
                                /></Navbar.Brand>
                            <Navbar.Brand className='email-link' as={Link} to="/Contact">
                                <img
                                    src="https://icon-library.com/images/black-email-icon-png/black-email-icon-png-1.jpg"
                                    alt="logo"
                                    width="50"
                                    height="50"
                                    className="bottom-pics"
                                /></Navbar.Brand>
                        </Container>
                    </Navbar>
                    </div>
                    
                    

                </div>



            </div>
        );
    }
}


export default HomePage;