import React, { useRef, useState } from 'react';
import emailjs from '@emailjs/browser';
import './ContactPage.css';

//inspired from https://www.emailjs.com/docs/
export const ContactPage = () => {
    const form = useRef();
    const [show_confirm_msg, setShowConfirmMsg] = useState(false);

    const sendEmail = (e) => {
        e.preventDefault();

        emailjs.sendForm('contact_service', 'template_79cthj6', form.current, 'HzvAIuQVfLgseKPr8')
            .then((result) => {
                window.location.reload(false)
                console.log(result.text);
            }, (error) => {
                console.log(error.text);
            });
    };

    const confirmMsg = (e) => {
        e.preventDefault();
        setShowConfirmMsg(true);
    };

    return (
        <div className='contact-form'>
            <form ref={form} onSubmit={confirmMsg}>
                <div className='form-inner'>
                <label for='name'>Name</label>
                <input type="text" id='name' name="user_name" />
                <label>Email</label>
                <input type="text" name="user_email" />
                <label>Message</label>
                <textarea name="message" />
                <input type="submit" onClick={confirmMsg} value="Send" />
                </div>
            </form>
            {show_confirm_msg && (
                <div>
                    <br /> Are you sure want to submit?{" "}
                    <button onClick={sendEmail}>Yes</button>
                </div>
            )}
        </div>

    );
};

export default ContactPage;
