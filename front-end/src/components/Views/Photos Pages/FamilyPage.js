import React, { Component } from "react";
import { ReactPhotoCollage } from "react-photo-collage";
import "./Collages.css";

const setting = {
    width: '800px',
    height: ['560px', '480px', '560px', '480px', '560px', '560px', '480px', '560px', '560px', '480px', '560px', '560px', '560px', '560px'],
    layout: [1, 3, 1, 2, 1, 1, 3, 1, 1, 2, 1, 1, 2, 1],
    photos: [
      { source: 'https://bn1305files.storage.live.com/y4mulXqLhwv2nZocOmvY1qgRoKYKurndoiqxw5StLVFiV7J04pePs0nJ1ApoZRR6rq_e18vRzjdSNeUzn7LrM9vZ2SDTgya1OWCdnAeYj_vQyBP8wwaQHPf2RGSgVtPJn4S9Pe5AZbg7I_rhf-Fig9X9RmESnJ7ywgIxf0lpFmG8ukWoCDMKFsUE42AZ8dJL1b4S3Co8k_9_q7ZFynrBA109w/DSC_3254.jpg?psid=1&width=5676&height=3784' },
      { source: 'https://bn02pap001files.storage.live.com/y4mrd-O7d2ijNWCQ-JL3NMK-Wgc_HooPsXum8hO8XSr_u_09S-PHgD1pTmE-M1V_uG_h7gYHAHarixqQWtnF9M200CInvWlr0EcDkfX8pBwdChQy3s2igWpkOr_DkNJtqqiEiE307Z-aMwQQbSXsWiQ30zRseDaT-rCYXjOZaC2BtQq-Utb8QVK1qVJLYvvDon8JDlfH8cDQC8IulMr2UbM0Q/DSC_3065.jpg?psid=1&width=2552&height=3784' },
      { source: 'https://bn1301files.storage.live.com/y4mxPzXbGWVWfoGE_qDUDe-ppdVNf9ge4ZyhG4qZyW06rZXaiZB9bSoi9NAglV3JJTEpHiLcZ9uOtmMihciKJXTlKciwRHkkqdTvdJfozGqABL04LNG4vUCRdW0LwlU8P6b16HKUtFsPJeQv0uOOsqRB3_wMPGo-XxBLsBO5wJUYfcU63mUYnZJPuI3wd8lY53CJSLxu67sEP7asDOE8KqzrA/DSC_3277.jpg?psid=1&width=2650&height=3784' },
      { source: 'https://bnz05pap001files.storage.live.com/y4m59yQyApN8n8x5mL8SVPJQLRGOWF3EnaRDIY247vbbzEScfhAf71ctwjuWTk267gNTh6L6zxn4iKXRKsUlHbXyK8CE35gXb06qlhNbJnmamcDZc1c6c51qSxFTFnV_OI5MjivdsP6XLzWPO2qiUS3chu5WrY_QlqL4uHg1dLslHryILoY-EGz7UHUo1jhxxu8F8UE_Dm1BkZJXSZsvGj25A/DSC_3170.jpg?psid=1&width=2523&height=3785' },
      { source: 'https://bn1304files.storage.live.com/y4mjBmPqQk16ACVteJGCm8BV6CpUP0_7M_IGC10L_w0AbM17FZookFl8o-5W1bSuNo1SulepgzhoVhGreh-zR9Q4POIHlIGL9I2__d2UKsLqFJrKSvpwEt7W1JQSBeAf2aELHro5mTMWk33dHcCTHOmKw-RJ8R59_ilNxXgPryXyyBx5DjWLUxJ64Fh2RrNwQ3XrfiSzTCE0sU_UWhd7LgpEw/DSC_3126.jpg?psid=1&width=5473&height=3649'},
      { source: 'https://bn1304files.storage.live.com/y4mLZs2OkuPZf8D4G_c7lUM5VAFhuq-Y8ei8_WqdsoS5xMU2En599mlXpFTEHv0gyCjCQ2PPYDQjE90_tHsehkdaJ55RkZacse8APLhtPatKVgGVRUxla9pp27xywURJHyMFVky16SlrvEeV6PZp5H27GOmwToiy5zwc0sIHeye4dyFsTSk0de3KK_ylBGdpVpvgTRZHKreMloNZeFc3YOA5Q/DSC_2282.jpg?psid=1&width=2432&height=3648'},
      { source: 'https://bn1305files.storage.live.com/y4mCHdbTJuvb1YUrtwyHh4cIQ3wHKxf9nMeJswB1-La2J2GLd2FpCP2lEXfdIEAHxNLJR8YkzPtiL6ZiDGS1by94_k-XNjN5GzjfxkqKG0SnL434BZy35zhc3pw2Anll91xz4_JRLEwKb6i51squq3PSGgdbiptQs0FwbnTEnqZP9iFWRN1GkO_rLrEkBBXFJjvnccLhSmtbZ6Kv0qa-68cEw/DSC_2383.jpg?psid=1&width=2432&height=3648'},
      { source: 'https://bn02pap001files.storage.live.com/y4m5yDzpeh6Mra5syI5c6ueNA10YDDVNfYNDLDw-txfp7NeiXDWkuS8VDb651gN6TgBAAujNbFY8o_AXH7tKuqDo1LnhD3s92BT-ODl4dxUVG_re4C7GcRIhHfcwMbt9FmEQxF9B5lcx988h6-ByvlfY1lBRwk6uX-ZWlST228laYoxW1ar4hNEvDWOOP3j__BATbJOW7nNs-lX0QL7-0hdkQ/DSC_2126.jpg?psid=1&width=5472&height=3648'},
      { source: 'https://snz04pap001files.storage.live.com/y4mKdr1CbvczH9n_MyFZCbw6uDd7O0oOecnYMW5WwVM85I9WXiLx3wufm-Yq-S47hU4hZt_AM3Nxf59OdTyQ3Ql90UB71Qb3uBkTNmiF9SSJKSkMZj6EUUeov-ecxN41O8F7SD6J29L6YKS10iwGk0UNSgylbsiT3m0JIJbccDYzUXR3UMuwJ1iDotAzENnwNnbiUgju9_gi9SEijSrW9R2DA/IMG_0231.jpg?psid=1&width=5740&height=3648'},
      { source: 'https://snz04pap001files.storage.live.com/y4mw3us9fUuxABncDFtETGEzEwgfWHFcvsFTqNaiMOJYqJJXiItuF_Fs743kcCBd4BWEnzkFV2JvaJ8aDIEQsqSV1CP44AzK9PS-_GOelBBqeFSmWRysSv8_Vo4i1-rVWlFMx0ijqcMNzgXmRsK0LlQXXh58XtLPHahTk5J7ZAB8atJFgU05aeZFo1prsanD_27hhkNnTrHELRfscDeBDHcFg/IMG_9657.jpg?psid=1&width=2310&height=3649'},
      { source: 'https://snz04pap001files.storage.live.com/y4mMXGwrSAz2NrAHyPOi-UH3sSSHqlmH84fvoSsFDDrF3qU9YQUCZP9EpinUhFKdRKtqJKP2TZEwvC_O9yJ1G8EnFVmd1AI-Dzn_5AiEt5r3qnd0F05qOAxF34E_J9TRg5bAgM2KMkvP64Soh_ycQGQtc5qX2t5h7FD5gWaKWD_WbZQalgcpVTE6SLvHX0P3gkMhCCRwZ31yN_r-4Mxzcw1rw/IMG_0303.jpg?psid=1&width=2432&height=3649'},
      { source: 'https://snz04pap001files.storage.live.com/y4mkmR00RhA8XAKFhTklLHZ0rAaAAPIuZypm7VaxehLweOa3uuC6e1MYJAznj-VscI6-dwVT4mJEkGPNNT7Jwg8wV4edcU5wOZmDD85XJ6efSGx_hdqDzb_JPAMbObwz89fGzQxBxpWVPbxK6RN-FDU0gXNKkvpC284lEx7Pw7uyl683Naz9Zc9n-78sqh5QGmiPv2R12TfbcbjrL155Q9okg/IMG_9866.jpg?psid=1&width=2432&height=3649'},
      { source: 'https://bl6pap003files.storage.live.com/y4mi_zRI88HVFFA385YViALqEHbplfC9AtECsN7FDh63eGa8udwpLpW56XDxLWEOQFtAHJZLCCsHJ-hsQ0yJZoYOFYQcUs9G_M1p1iHT9evwBsaCHFrdjAgMi1CDZd4HUzUlef35ipTBH4WUSgCPjemf-QyBHy6gegcAMUm3sL5d2GjZk3ypncQXfWCQlapIhht-ZK0uK-ZeJO-k4W1DIgL_A/IMG_6940-2.jpg?psid=1&width=5472&height=3648'},
      { source: 'https://bl3301files.storage.live.com/y4m83JuuKblK8rhEyVy_GT10IRuFIyuJhzyht7NUkygt_Op8sTtx6D65foTwgTbxmwWKZGWK6LrJvP-lTNOsf-3sBtUHIHFjz_8rN8Foc17Mz3IA0Q2y5fIXSsjYHRHGLNRuUtAaYQu3Y6KdwRMcRBbG0jGralxeIR_biGlY4V0nRU_cajAnZMuj_HAAtuQq2b4OMDzs1EeLs_g3eF4Ibgd9Q/IMG_7052-Edit.jpg?psid=1&width=5199&height=3648'},
      { source: 'https://blufiles.storage.live.com/y4mo0IuhVuCMSU0v3UgeJ0RVhJibSwu6mfiul17fPJ5LogMEiNQ6axldNiAK49TI-CqgpCSWzkSHjz5Qi7li9s1i5dxqVb3vPRLlBXAIGGavDhxlAq6rD3ty6OGbEkEJ3VsWk83nHSu5Yln1pkhaOcTuEj56YR2pFRa_rS2O07V1KJDg2LmZYKmZn37pMbhFYqSRAKDpGm6zIDIBbrpgd7aBg/IMG_6819.jpg?psid=1&width=2271&height=3649'},
      { source: 'https://bl6pap004files.storage.live.com/y4mVnLY00chPi8bDgTqhglQ9IGTZMlL0rMgLkp_4_KqqDNFMtAPuhlWnPoItVV6vZaxLweIYGmili1L0gGH_acJLKu4ckp_q4AEiRS72kejcVUQSsQV8ykyo0ue6ucarPy6ttesju2xOk7qyXW-wRgCacCRM6_-3Zehom0CSmzVlV0Pi4W67Md19CcepdlkDP-haLtZAOdbWWxFkA7clmucWQ/IMG_6855.jpg?psid=1&width=2432&height=3649'},
      { source: 'https://snz04pap001files.storage.live.com/y4me9rnlvybePszi6XYCHnhxS3XQRi_T3srm67qKjnaZ2Nd2jbxZwpZy5XCGzEAtHK9okc6AwMZUVZgZuxvv0dMvV4zc-WZSXQj2LxE3Ix6Szsz9j-JifbYPI_as1H786qTXFfLbt2JXH7Umr0CCDgMmPvd2eoGpo0n3Ki5MfHhVt0KkMhH19oxz2Rgf9xceG17hBdblFeyr6tkwb9X1zmqnA/DSC_0562-Edit.png?psid=1&width=5754&height=3836'},
      { source: 'https://snz04pap001files.storage.live.com/y4mJc-vABBXnaawPMBBriWBVIg0BTRCQMlXWE1pLRNE9Lgwy157-XMxA1FHDqGOnUkkbSVBENYsxTTePUBllFD9ocSl9dZKZ6ba0aE7tq3YR5L3TDIgC0XPt1bGcMMz41v3_1a2phbLasv366r8sDzNHQPeb5w9Gw-5_eWKCh5GP7CaETw-v56d2Ygo2gJdo0eG8hVtqMdkf8ZQjf2dWPwoWg/DSC_0488-Edit.png?psid=1&width=5754&height=3836'},
      { source: 'https://snz04pap001files.storage.live.com/y4mD1fC8oGiN7zHdjIgWRJdSLfBr-izkcTtJMV54bl7sYA_3nU69CzfTFTC0cGDDArDf3QCljFpoisNZrK0wfNkoObBag4QDtLJx2b44I8QvNNjFRyMNfVZzphmxoA2kHkcHVCgcxfF54hy5y7q2M-7KKuYYor6xfcM_v_wAap8_RUEjCsKcw_ZgJi5_x0fszNnRWfm69CIIzc5dK_aZfGmUQ/DSC_0192-Edit-Recovered.png?psid=1&width=4567&height=3045'},
      { source: 'https://snz04pap001files.storage.live.com/y4mu51bB-hyKPU7-8Po5srHH8SRkXRwBqJR1BMWnp9ftMcHweBDH6_0wr9MC_B_sbghAWzlYq985dH5l0OoPqAQIIjaN1SGTYjqi0oXkj-_WOb8ACdX1ASYc57mZB5kpMYcBspAMHtbJ21IN_N9TIQ8FlQCm2wk2wIjPKm4sG9e_Ob-0zda-dQbGFC3DOwKeFatVupHO4ZilYUbK19RacJH9A/DSC_0217-Edit-Recovered.png?psid=1&width=5754&height=3836'},
      { source: 'https://snz04pap001files.storage.live.com/y4m6C0WknpEx55oBW5u7zMsGZd2_24gqMVCXGmaFFU8irgRgaM_flxfZCIF6d7nTSYgr6IyjiAU9yMGF-TebGHoVSCNQYWRF9A-MQFdgeNbzcZ_26gga2B9IBzSJTYqb1C4JhUq7FeHskMu_tPaBOnjXmVztgOiE6a13uHAthoLOgSdO98B3ZSQhpylsQrYatzzfcB3ydhYtBH6XBSZy4luwQ/DSC_0157-Edit-Recovered.png?psid=1&width=5754&height=3836'}
    ],
    showNumOfRemainingPhotos: true
  };

class FamilyPage extends Component{
    render(){
        return(
            <div className="collage-container"><ReactPhotoCollage {...setting} /></div>
            
        );
    }
}

export default FamilyPage;