import React, { Component } from "react";
import { ReactPhotoCollage } from "react-photo-collage";
import "./Collages.css";

const setting = {
    width: '800px',
    height: ['720px', '480px', '480px', '480px', '480px'],
    layout: [1, 3, 2, 1, 3],
    photos: [
        { source: 'https://bl3301files.storage.live.com/y4mlNW1ipbRuycu1bmnOIfr88NOye5DnafHT-T1hE-ex-0VvDROakT7R53MOvIDtmt1Tn7JRCsWkXOnGs4S4VOKvlwoHAFKVz-N_RE2a1CKu3NRKSl3nX_emZocBHZ7ki9hwRRmLge2kNusho8nI4vBg2_zIVqQUiRcQqKzuhJ4tidzd0U0LKYFJ8eH9Jd5sBCdlbxfJOOHzeVcG5pdsCmPJg/IMG_0235.jpg?psid=1&width=5676&height=3784' },
        { source: 'https://bl3301files.storage.live.com/y4mu9CGoP0sl_AId0XakZMxNLRCBzlP1RF0G2g3bWqZRXtBw_IdC77ChsiqyF6t-ndRP-o60knFEpb94gKBY2s9IJrTTF21Sa7NrnKl4v7Nxx3UxunIyc9xKAWkReRZX8YuZF-rkQyyuia737ftGnMoKIUj1KOd-u3QkT-9n4svkxbBvhkx15c1XhgxKeN6suejNp0vjNjptpLo4ZF0vKdnUg/IMG_1145.jpg?psid=1&width=2523&height=3785' },
        { source: 'https://bl3301files.storage.live.com/y4mAOLnvO-bJ2gION_KK60xWut9134YAUyatjRmfRpUBJTGWLmrdOLHdGFt7oMXq31E8EmHmpCJmvgs5nEVxcX-9amJTyBrQQ0PZJuYdO_WB9QZwBh4QHe5MKRWQVJl1O8onOVdk7oPJbwAPHCkKGFiTFELyZARWMrxm8cafxgNsnJibfHP_DGFTejDKCh7hvNT8MzKWeUIFOXTm0QSjbu9tw/IMG_0979.jpg?psid=1&width=2523&height=3784' },
        { source: 'https://blufiles.storage.live.com/y4miuFfJbT6dzc5Y-NDNN7HYhCe2hITNeTy0_H_Zo7FLc9P7tNMZ66lzqHekcg2jZEqKxSBA9lTi27JMYpNNnf98VY9oHYoTW3xqmUECuFaZSXtE8t5_Jv8VC3IfWd5J7WpwKkELzYBt5AIWWW7X7GHiC-cmtRutfMsJLKkzTcGzyOHFEVgKoWFTkVNkCkqG6hNF5wME-1yrC-w4TWEfQKumQ/IMG_9731.jpg?psid=1&width=2523&height=3784' },
        { source: 'https://blufiles.storage.live.com/y4mh4eEcGQoAPnpeb8krmhcXhgU2A84D3sznBCU8k3xjKNMJ9Poxu8hmsqUJz1vhM97gZS3Tpgn_86PifXpOjUTtWG9i_Z2JOAfKz_p6bGHXfVtya3Z1Q4daEVk9wybJQxk0tEEAzSPxEJXaCxixY04Ag1B-1WcRE_h89EynY_U5FFMczSEu3UkA4SY2hK8Vneo5lLLLZoC5EJkoHTtZDmszA/IMG_0266.jpg?psid=1&width=2432&height=3648'},
        { source: 'https://bl6pap003files.storage.live.com/y4m0JoCr_pm6aWbDWmw9qozF-CAdlij5gmuaSUJ5DklnAYgRT8Ma6mjmVcTLTWoCYBKPJ5Nb7DtbFMbhe068xzWPybiLP6ynh_r3g_S6nCN2ugFQxXAB9qOMROSAH0EBdINFTNV3DHb7Z40KrHUh5Je1RoN2sHQ84eutLqetzcfjo89HcBXCEy8V3Sv3n3DPwBtdtIktYOAkS-51VmyCIrD4A/IMG_0105.jpg?psid=1&width=5472&height=3648'},
        { source: 'https://bl3301files.storage.live.com/y4mTp6JDzVlFddGreNCQuitEqcKn7s9_AqAsasZXfvqb8aQLRkthOD9KmdVDfxlVw66aoFWxA1JGJEfsmv4RNAmpGafB-quhF6gk0hwvsnY9IUP0-pbsmCKN8GBwrSSqyexGF3yHNBLnUBO65TxdJVqxfP6xbP0uQEbCRXLXx41oX7cgIj6QFmKTDzpMtZwLFSIAI-3bo24nxZRMbdZhZihqw/IMG_1494.jpg?psid=1&width=5472&height=3648'},
        { source: 'https://blufiles.storage.live.com/y4m3WhgDQWo_FJ3lU5yZtIjBliXoDCaXcdFlTxv6y4nJvHt1cCSSL6P6VRJJ7CCAGVLrLZxBEsyDecNNQ16AqhlFZ-zDpazFWugs-r8wV-tNNNoCu0zPuasSCQO5Kvx-bSKzOAySUgAT7WDh8hUAC3Q4hAMBeC5QSiLg--Hl_UGduUUtVVe00p-GFQAbjp0W-_eFPrWW4LEZT1zIUuhsImJuQ/IMG_7419.jpg?psid=1&width=5472&height=3648'},
        { source: 'https://bl6pap003files.storage.live.com/y4mcHN_nC45QkaJlgE_iYSyjLknJmnZDCrmBUAFO1FVGR47FnXDjA8YlZiS8mU89_8QFhuDwp59qDttevaXlBtJYsBZOLwjX2tVJqqoiZRp86pz-DYBENJoMnKBlPnyQPks5sA6MmeEptqOP8F2WfWbrw8tc44KJRomCDUPei2KC2mhGb9TReWUDPRlbGD7xZAQIjtnvLKJybTECgo5vNuKnQ/IMG_1452.jpg?psid=1&width=2523&height=3784' },
        { source: 'https://bl6pap004files.storage.live.com/y4m_Xf6G9PdYPld7wSP4ZlLxwQSJJ-b3mfegoLT_f0gF7ICcM5BW_TzPR67OWaoDmmIHc4DPvAmw54dGF8n9hn4_J_M3bpFSSisLI5rRvtWm4RXFSSpL37t-xkiARCm3Jnuxzj-qmq-CFpKLdkFiP00ctcDh9YPdpuz6uqebftNx8HibX5AJY2o1y9sY729Eosu8ZQ0VCjHtN4pAstzZemTnw/IMG_9751.jpg?psid=1&width=2432&height=3648'}
    ],
    showNumOfRemainingPhotos: true
};

class WeddingPage extends Component {
    render() {
        return (
            <div className="collage-container"><ReactPhotoCollage {...setting} /></div>

        );
    }
}

export default WeddingPage;