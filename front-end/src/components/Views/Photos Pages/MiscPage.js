import React, { Component } from "react";
import { ReactPhotoCollage } from "react-photo-collage";
import "./Collages.css";

const setting = {
    width: '800px',
    height: ['560px', '560px', '480px', '560px', '560px', '480px', '560px', '560px', '560px', '560px', '560px'],
    layout: [1, 1, 3, 1, 1, 2, 1, 1, 2, 1, 1],
    photos: [
      { source: 'https://bnz07pap001files.storage.live.com/y4mvWezYcOtqW9BMnOGCrqURWUvhhQ9TzKZrSg855uBnE_CKDkt-eMU2EXHPvyPOWt9f2BL_YYtRlZKZTqx_0Gv6o1qJizoTSZ7f0VEqTE6ot5kV2cclkZB2jlCfYF7WKIlfx7t-Z5C1vAV6qGF1_mdll5X1XkyyFjbFhguRO-4WxEUNGWhgKL5EzCuzSl7OXgr6CPLZm33YRt-wvdQn0eeIQ/DSC_3135.jpg?psid=1&width=5676&height=3784' },
      { source: 'https://snz04pap001files.storage.live.com/y4m5XUmCWp_zDVPukgztH4iXa07ECiryvcaJrRlxtPwIH49kc-aHkYY57WpWnPr3NmQ9PyJG1RoFSpIXSBqj3uaGz9ql5HkLZbuUgXXHxGNPtu1wsZFya2jwfWY0AjJBXhO4iXusbvl1Qt8WKO1egw5qiZB_D17ukCSU3UE53lqPCyQN5B_5Uoaen1YalQ8nXmBM_NXE1EDxLPgnHNif3h8nQ/DSC_0210.jpg?psid=1&width=5676&height=3784' },
      { source: 'https://bn02pap001files.storage.live.com/y4mRSuSB4XbYO1rygwrNxKtM3T_qQbXRdo3oT2UtL-c7TTr3rUe0cBI5qiupipGkHG6rf6OPp3kv3swF6DUhLsqB2L9jhlhMN00VUg4-1pR3ZB7gASkRzBhCrZ0qOFZHD0FjkN6nrDw9cHH4JM6hRnn0u99EflDwKl0fVLlBRppN0EdnQl6uVxixRyhqwH47V5_s4U3JmicURk0u4y2Db7gLg/DSC_0131.jpg?psid=1&width=1194&height=1790' },
      { source: 'https://snz04pap001files.storage.live.com/y4mxcHmMtOU38vexIpH-xiDJtjB04BcFpGPwlXby_6Cnl8lNe9H2CFPAl-oI6vhs9_srOnlW1Gzl2Vaw23_CK_vmi6M__rV91yugoXG3kcvzmFANTaiIZF8u_lBDN91jCiTynT8DWRcGHDXIpZVgVPFCUn3AyYcVZVT3yrzuo_LK9Tdyjk0IrKvH09Kfj-eH8j21lCzTJ3r3LlqpEowT799lg/DSC_0156.jpg?psid=1&width=2558&height=3836' },
      { source: 'https://snz04pap001files.storage.live.com/y4muauqHtCm6S0XpBV3iOkA0BFI9HF8M7TecBHkhbDr4rvoHB1qiD1otvXtjncVynXupzyGDoPEL_9yDZ1prBOHuLBhreKxwIvEmJWYo479VmiJG8YdJW8MWtScznpUatpDphI8Z_-uBUb2FpxcNxA4rENdg_4LHod5poNywyQd1PzBQ3OuSJNxoDQ66funFUFFGqx5iDJNppf8tYpG-nfALg/DSC_0166.jpg?psid=1&width=2558&height=3836' },
      { source: 'https://bl3301files.storage.live.com/y4muUjf6T42iP0UWavr_NRYjCxsXJiD6pD4U9sXKbRaGF3qNgaeonqtvOrncgPewZLzKRg8gnBfnFgKzXM5wdkfZd_SdP8VRpOkF4pIn6E7wrNg6HqiqHV5zvZPWJwn12SZnfnR9_3SE0yMzHnJ1qLeoNoluNwdy4pgh0OGmQL4Hwr69sTSWcjG3IHxSbOofGUgPvlw3aTEQDyzspjWC4vj_A/IMG_8803.jpg?psid=1&width=5472&height=3648'},
      { source: 'https://blufiles.storage.live.com/y4mgY3JVe2-EWRiB2-lBl4Z3twhkvOolyeBhPLP5Vc_ABtRx-tG7BrNehhYuCg7IA96W8kMoAtzh8I5o3wnzaOkzR0Y3eatFmJt6jLKsBD2S-DnSZ5LffZPWYiszWWnsNlbrJ9hV8ZGr0eUYsmekiNmJNLrP9m678ZNBaDSZAsqJ70GUd7bsU8NW1PsDPOng-5CsqEEvmjz-WBZJFIr6mCGwQ/IMG_8836.jpg?psid=1&width=5472&height=3648'},
      { source: 'https://snz04pap001files.storage.live.com/y4mqN9y0mA4ZTF9YId964SPMdXUNbSIHA0wxKldvcYTpjMtxgi0n3S8t8ntv6voLvsPBfKGaAsMMGw7_sYRZdyD96oWyaYIG0x27dpcS645_aJT6V9JTFrHCPnRzgm7XKZnq2xXCTcfVvj2jAOhdgTqaB7dheslCVTKwcKcKrmbaEQXcpSfIOhxBOFgxusOwQ-0cQxEH-VqCIW9HpXUqunTiQ/IMG_7189_1.jpg?psid=1&width=2558&height=3836'},
      { source: 'https://snz04pap001files.storage.live.com/y4mbZImnR7jDqxpKoj2c8gj-JF9RCiCmxIB5uQVjiXjzvggYi6YPo5w5fKGxgTKvEAJ-TyX7rnWWG0ZTKRnWjKEb_EHY0t3JQdxQunIDWSb-PtrrUGXdm62cljvpUr9dCRvBNEQx7zQvEqBrNbgjjWcoDDA5Y_HNtecDcDhAilOPClIYxkvL5kz-3RpRueeBFODFZs4oz9j6l8WPomL9jHICA/IMG_7195.jpg?psid=1&width=2427&height=3836'},
      { source: 'https://bl6pap003files.storage.live.com/y4mAtzqxJG1Wm5yZp7yLMrFt1ylCQ9uIJ6oRngBKCdoi91lqVUwFTehduEUi6dXp15qwIWzIK5h_d_bYxiUDEcyEvxr0vwzkI-6hazn8TcH7xYdTtq5Dw7LWrSdoZZjfhloqXtoK4h2fXBD4j_Ra2WWDSj9e1MHhhn7k9kDsJ41s5JQG5sP4T7LpQbLNG9caTsMBHFY54iW5gcPEPylSo184w/DSC_0604.jpg?psid=1&width=5472&height=3648'},
      { source: 'https://bl6pap004files.storage.live.com/y4mgUO3gtPHkKBMQV45qHv_Mmdu_z9kMmd9uRZI6VgEi5vjok6jzOctKaj8snS1zU5H3b8oVRMMQB-yVFWzqB-5HNqDSuunR16CTgcpdz6z0T4yuxVsD2tZ4MeZystq6-6vMhq0rjypUI-FyFLJfKAiDk9UINWWp2gQQjBniKzPaIKqMLMZ3ZOck4caSabu5ioH_i6viCZyKAvvaqaZ1k4Bxg/IMG_9296.jpg?psid=1&width=1369&height=912'},
      { source: 'https://snz04pap001files.storage.live.com/y4mLorDxxaP6K8AUDuNEOS5aGRPyR1w2GRlpReuCNfiTuwUNDhwjJA61UdrSDFxEoWQ5O0t5UQYmToiezJcVEGCYfKO4tP8hzeYNGZA62tV0OYMJXlWFrj9BM6XhPpZPK3AS7yXkS8x4hwZTQfXqVpcjwgJ4MT2KupK8IcDQWhODdSA_bqVNReYM2s5wT1oJIZwjvfb0Jf_YhY8GW2e4wREwg/Makenzie%27s%20pick.jpg?psid=1&width=5754&height=3836'},
      { source: 'https://snz04pap001files.storage.live.com/y4mfbfIG9-zUNjne5EJynbippbfpNqnTJC6aeDvbp9cycd2mVxMRu3JLJgBJarClsa7MLvJeDQJLcHaujPlct4KioRiEaqeuREPDgr8eXm6w1Ufyuxgo3u5mu8vHBht05OGQUFbScTFE5nR2IYgbbSlrZCq-vLc79N3PQ3Ql1WD3CftVmc6ukKrO_vYxP03lO6Ie2rMTSDJ3t82JyehrSMpdA/DSC_0155.jpg?psid=1&width=2558&height=3836'},
      { source: 'https://snz04pap001files.storage.live.com/y4mBEl_Onr4hZYYSZQRSoqqbKDNSKPLvWTK40kcrKFprCRU9EDGpAAp_k7JSBuTBnIUfKR6T85GElWneFW6FChTovbtxrH35bPz7QU8Q3YENlos2Q3bvjt7WKAW4wzSpZH_dD5x0slaT_dWyyhYQ7orkAGxgf2YsiT4bhwLnV79qJcOQAughwiWZw5oM8nPTJB8GQeST3HklwPpTKLmmejllQ/DSC_0094.jpg?psid=1&width=5754&height=3836'},
      { source: 'https://snz04pap001files.storage.live.com/y4mMVdGcXrXjs-_i2TpH-XXzTQp08B2JCtuaFzM6iuDBcuM2yjbgF0Rqwkhgpw2Hkx-Eaf5OpnUVafnk1pG6UMQmbpIBQj8lK4mf0baEeCZDGHDffcpmmNu_jAMnwt2fyiVC-u-XbBZufREviWSgT80go2NjIF0rg8Xs42hnqvq9mXFyEm8tRO7ma-nN6TjwbyRZYaYqK5S5UQul-r0fSgD5w/Jordan%27s%20pick.jpg?psid=1&width=5754&height=3836'}

    ],
    showNumOfRemainingPhotos: true
  };

class MiscPage extends Component{
    render(){
        return(
            <div className="collage-container"><ReactPhotoCollage {...setting} /></div>
            
        );
    }
}

export default MiscPage;