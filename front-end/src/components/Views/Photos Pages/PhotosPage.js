import React, { Component } from "react";
import {
    CCard, CCardImage, CCardBody,
    CCardTitle, CCardText, CCardFooter, CRow, CCol
} from '@coreui/react';
import { Link } from "react-router-dom";
import "./PhotosPage.css"

class PhotosPage extends Component {
    render() {
        return (
            <div className="card-container">
                        <CRow xs={{ cols: 1, gutter: 4 }} md={{ cols: 2 }}>
                            <CCol xs>
                                <Link to={'/grad'} className="text-link">
                                    <CCard className="cards">
                                        <CCardImage orientation="top" src="https://bnz05pap001files.storage.live.com/y4mtWoabKH57pqQwj2zHLmHCC5eY--SEBCnnm4u5xwF4s63TznbYQJOiKE0U_EbG_DhKP4FLst9PzoObfeS-zkNiN9gqVYb1pDgq_N6EyvEuH_oPQ3A9XVUYrZTr-YJ4Gz3pWxf6Ag7gqGIYF6mmw9ot8M53VBxA8Wzgh0lcr3vpjR5wbT9XwG38mSbUP0M_edIy9mILumtrkn1sG63O1SmGw/DSC_0290.jpg?psid=1&width=1419&height=946" />
                                        <CCardBody>
                                            <CCardTitle className="titles">Graduation Shoots</CCardTitle>
                                            <CCardText>
                                                Show off your accomplishments through high quality shots of just you.
                                            </CCardText>
                                        </CCardBody>
                                        <CCardFooter>
                                            <small className="text-medium-emphasis">Click for more...</small>
                                        </CCardFooter>
                                    </CCard>
                                </Link>

                            </CCol>

                            <CCol xs>
                                <Link to={'/wedding'} className="text-link">
                                    <CCard className="cards">
                                        <CCardImage orientation="top" src="https://blufiles.storage.live.com/y4mw7iUtquAoQL95l1CPakBByTi4YTOcKWtQzO4IFJGJOxaF80dzvHBITGqTxkBQEsVtG9Qc4iWXN5GUNXwIQixYBOqSEhEKltkUxdDee8wenGCN-TSyjpMsxT5KpDT5ytobluHxLAZpuakh5EJr0QnDVckFzktmsN5fWpiQRM2PVCXGt168Cdd0kmuD5AA0BtdQn44Ngclt1dQNnvDOkWkHw/IMG_9820.jpg?psid=1&width=5589&height=3788" />
                                        <CCardBody>
                                            <CCardTitle className="titles">Weddings and Engagements</CCardTitle>
                                            <CCardText>
                                                Have any moment of that special day captured forever in the best way imaginable.
                                            </CCardText>
                                        </CCardBody>
                                        <CCardFooter>
                                            <small className="text-medium-emphasis">Click for more...</small>
                                        </CCardFooter>
                                    </CCard>
                                </Link>

                            </CCol>

                            <CCol xs>
                                <Link to={'/family'} className="text-link">
                                    <CCard className="cards">
                                        <CCardImage orientation="top" src="https://bl6pap003files.storage.live.com/y4m1X4OnoNVsl470jRVUgoTQdVl43qnZ8t6QM7g45LjJkzEfRj8PK22w4QYSPWuCtOWqCSVFfjQyE81iiHZjNEvh05U3YcEBk4Y_n9UkJ3tXdIGY5-KQsZyZuK0trdrOGY4FjIfgmG7FWXjYeR6dkxW2UCST87qdOUNRcetFJytgF4zFbPXKFdg-a-oNWsvpB8W9GmlLWuTpmSvQkDlXO8p0Q/DSC_0571.jpg?psid=1&width=1419&height=946" />
                                        <CCardBody>
                                            <CCardTitle className="titles">Couples and Families</CCardTitle>
                                            <CCardText>
                                                Invite some of your favorite people and capture some unforgettable memories.
                                            </CCardText>
                                        </CCardBody>
                                        <CCardFooter>
                                            <small className="text-medium-emphasis">Click for more...</small>
                                        </CCardFooter>
                                    </CCard>
                                </Link>

                            </CCol>

                            <CCol xs>
                                <Link to={'/misc'} className="text-link">
                                    <CCard className="cards">
                                        <CCardImage orientation="top" src="https://bl6pap004files.storage.live.com/y4mYi0QBGFKJ_nVnMX9uvMq-kx3lp_heGX1AHTTjsnYkpvVlHquRx_MLSKBQ7fjzu-79kpLUMiawZu_qqX3yeSinSiWnvjn5jbs8JUYsK01JDk5zKARn7ggRAojy56YjuBqQN4oFtKuQGstcfYQ1K6Pnm5jL81xms_k3WdIVLUpi791oLR5RMOAsOk5VpRU4E19b86FDrnh2TL5CqZYjbp4UQ/IMG_8862.jpg?psid=1&width=1419&height=946" />
                                        <CCardBody>
                                            <CCardTitle className="titles">Portraits & More</CCardTitle>
                                            <CCardText>
                                                Get some perfect captures for any specific event, or if you just want to have some
                                                fun, we got you taken care of.
                                            </CCardText>
                                        </CCardBody>
                                        <CCardFooter>
                                            <small className="text-medium-emphasis">Click for more...</small>
                                        </CCardFooter>
                                    </CCard>
                                </Link>

                            </CCol>
                        </CRow>

                    </div>
        );
    }

}

export default PhotosPage;