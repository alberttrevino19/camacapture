import React, { Component } from "react";
import { ReactPhotoCollage } from "react-photo-collage";
import "./Collages.css";

const setting = {
    width: '800px',
    height: ['450px', '450px', '450px', '450px', '450px', '450px', '450px', '450px', '450px'],
    layout: [1, 4, 2, 1, 3, 1, 1, 3, 1],
    photos: [
      { source: 'https://snz04pap001files.storage.live.com/y4m6sVrpfacK3E_PQtDTpGyKdFlTOnypgUBhmvkyamsCV-OmKGahxXZvCLNw25aagY4qwdaVqMeefKo3KG_olR0KgRJsT5w_W740OqNx1LcxKVz6RyfxhzY5JGZ66aD0ifVg7zEj0_RD-nfKMuenbCmth76sg00rvs23DvgcJMJvFj-jjrO-hfdcgdIhymJH4wWp18_khqEzmIzefD-pyTryQ/IMG_0029.jpg?psid=1&width=4942&height=3295' },
      { source: 'https://snz04pap001files.storage.live.com/y4mg6eYtge5Ca-HGsCZTfy5Rq-C2ooEOhrbCCa10Ha7DvgYx3F5q_qYBcUnptzIlAT65RXFfgxbMjbCJG5-JfJl0ZdPdE0ptNABOOFppW99YCJrZxRS9cO9vllCr3oySpPoWgBXNavVV9OZ1CVvA9ea5pWaVkTX5BGSBCgvqrfVd7NJDwEzwr5hCAupKUuX-DS-P2KWDfvzC6QRG6vA8cx-Bg/IMG_9827.jpg?psid=1&width=631&height=947' },
      { source: 'https://snz04pap001files.storage.live.com/y4mMK1o-OfVXFhFxTEMPpfORX3_1zRagrVCWdPFu0bGvNhYkhj-wzoybXEKzXnMF4wDZjUWrAEG77rhWGXL6E7o5QIqILU3Bx_NSJ1dCqHbY0sl4JAr_pzPlMMe6StdOkEK19zot4i0m9Dl1kMUpLPgaJBUUEVe08v43HY4Xk-3YNssI1ttb0vmx6O_p8ioJbff3XnoG7vshKTLprV4lOBpWQ/IMG_0127.jpg?psid=1&width=2438&height=3784' },
      { source: 'https://snz04pap001files.storage.live.com/y4mkeBBK88bmUYi14HARwqBfPuG0atrYIrxVDEHcGX8r2GjYwzzHZ1rGY6pfpTuaWT-soNRwDqdLLAL6luYZjHM6-qhYAheF_jFJquZXhTfJhcI80lGYuTfOQmt1ZW78FieGTOACBch9NJ_VGylsu_lLLSN87Qxo_UjQwaXuzdAiAUKjT4RL0Ed8EqE7Q-MwkzwPKk972UQsW_qPXUOXtRYhg/IMG_0152.jpg?psid=1&width=2523&height=3784' },
      { source: 'https://snz04pap001files.storage.live.com/y4mbTVLEacyeirRto0Uk9lVy9qRydU9PVZzjjeMgGLcLmiXq-ctz9Ocb2NLbfM--AGKPVVdDjDTXR38a9S09m0BNO2aCbioCSTcKTHg0ZcT-xb6HR8PFUhB7MfFyyDCnX0zrX5XKofDvt-5YYKUc4dercK8wORyocVvbB2-2fmAkbJtjgJAOoPnj4_aFtcVAtK2l5Sf9UL1khBoRjOQaeIZlA/IMG_0175-2.jpg?psid=1&width=2353&height=3784' },
      { source: 'https://bn1303files.storage.live.com/y4mthiZDbdYJSVoqSQOutz19AJfrrYhd40i_G-aRnDGJwppEZXO7C1fY5G01QotbCZaSwvNninCf6NToRHj34-Y1PG_unEGaWrJwnhv9f9aL8XYFwua6p8Ej3dbg8yIvKlnuD0o5Z4w1AFbfBSUwh-V3s-hf7bCk1XBa37fJaGoTLYpQ4ha8cNRfklSn_lhebPxKeB9Gvyhlhd35FlKuvP-eg/DSC_2190.jpg?psid=1&width=5496&height=3664'},
      { source: 'https://bnz07pap001files.storage.live.com/y4m4jjH_wYDKZe2hgm1BhdmQY046DamB-6T-xdyLHKlWity8KyBbJo83Dwm5KVGcFIRsPNXqM-DPVu_RI_mpStNHO6uYYcy_cQZgft-LdtlaY9A7FUZienoArehYIbziUvM7n7nZ5jTlizqUqXbuoSrZ3xE6Mwel0Ca8NBtLrU9lASrjD10wG5cTBMUhpLWGPWgTqAun7QMfAKPfP550UDJew/DSC_2182.jpg?psid=1&width=5496&height=3664'},
      { source: 'https://bn1ap000files.storage.live.com/y4mMvuWr5-NrAKcbu0I7j62R0g8weWTVATWryD-oBpwAos3H2p5KbBD3CPhEPHNPM9K044TO729OuJsg6CVx6jA0fHAYbV4PrHnrsUYXgO6z5WE2-aZpod20kdcUdrna_T0QrMe3HJCt2KDdAV54hs522Fr6ZS6JSeoftmFmy96Ch5OMpbYUwoELaKWtMkUGQ54hsW0MFnolISUjkqT2XDLhA/DSC_2134.jpg?psid=1&width=5496&height=3664'},
      { source: 'https://bn1303files.storage.live.com/y4mIJ7AyNP_ynUvN_4c-y3ztU2yiWDShfgZFM2DiPW7I6N_rVvQuMoRe2b6OpoddjZ5Ni4q8cP0pY5Tow5zEitwV7qGOKnkrL_ti3QFo5YIj8KmOM6i9YBQ_cE0vOjCgTC9YeRJurnJZ3pB2Z8k07g_NkxrSe-8YGbfogM9Vec6JvNAUHXFJLzIfbsgI7ey-Dc5QEEkBUZNkTUbG-7A-MXM2Q/DSC_0318.jpg?psid=1&width=2432&height=3648'},
      { source: 'https://bn1ap000files.storage.live.com/y4maCDmyKE76K0T7dn1s7QbEUfhJi2dfSC7ZjGxTBuUdoYFvazGEi9U1CvUP7W7Cu8ykC6gEqG1MNsXbGd50F1t5QiGNOk7mmMYpZNzu7Fbgv6oxWppJRlK_cCwiAAC8t7pCA-ECWZ7POKB_9raRawumVq3WXKpILC6CWAPfcILgOyFSGwob6CurYRvGTEl8N_2qeHpfn7QZDeoo-Qq59uWhQ/DSC_0295.jpg?psid=1&width=2530&height=3648'},
      { source: 'https://bn02pap001files.storage.live.com/y4mUzkZV5Kh7DsxZiUQnL-yrvFChzKNBW_lo3HPJPkRA3ffOejEzd6EkKpRHIHjI4OMBPyuPlHxXz6ob_twyOnM50OgjCfVoERcRMRL3d6qQ-mck_xbuaJa-5ECKuWZtrNdCMsU7k62kSCewPzT9L8zyiNloxiPQh2D4hPTDDm7DGD3Qdgu41PkP0uTD-EaOeGUbkifQoIYwFLgqlK-izofhg/DSC_0608.jpg?psid=1&width=2432&height=3648'},
      { source: 'https://bn1files.storage.live.com/y4mFRRc8Cg-URJF8gLi7VYuzzC4u_Wy62tkTL8sZmoUNN2b6f3yxj4Y9fQ84rsYo2fx4GjXgokl2CFAC8_30rdZSZCWo0HtR1K_Tf99a7mjI8rDYikMJ5kXMJ2OMbETwoROdjLaNJ2BORR47HQ8n8m_5HKCDCYhusJJYIRKbx5P1rGQ4wdTmllYtSN8BEhkJyV9nZx3_70DASRYjo_6iwzmVg/DSC_0325.jpg?psid=1&width=5472&height=3648'},
      { source: 'https://snz04pap001files.storage.live.com/y4mwy9zNue6mS90YDt8pAh18zKTxJCFhspVXgHGBmpctqfhxD_VL4OC1yPr3BAudsHyLfRgaKHjEPXr9WqUW7rcfl55db8S-We_M9on1KzckXZ0JNybsg2WPAAfcYpvo5Zh-3atvw6whsa9yh7KmADoXtyZdfL52lGEXEPKoleIHuw6MO5YntOquvn1X0D8VNevCOMg18pul4y2Xiyi6Wwcbw/DSC_0233.jpg?psid=1&width=5754&height=3836'},
      { source: 'https://snz04pap001files.storage.live.com/y4meNSbyoKmGalIpR5ffE4ymWV5zv8_8KZzwK8ZXEnuIh9cHE-eQmp4mNhHq5o_FZGn6XwKHO4_uMIHGBsfjQzAVGbo66D1CBtLEtjD3wGgCtjrGJY6tebM9yXg6y21gjnppNzf8Sl7UFH7UxSTWxtUiVV-3b7nQbnWIfa28Bzp_fHQTfGUwzSwRozobA-mWQWosxTaEA9cDB9Cfc6Kk-wUxQ/DSC_0517.jpg?psid=1&width=2172&height=3836'},
      { source: 'https://snz04pap001files.storage.live.com/y4mzBR5Roxfl8w6ggIR2dFRlokhGH-R06XidawIgQRHv2t0k3fMCfMVoyK07Vqg-IZi1ccsH6Mu6pE7A6439xYQsVqso1XElSTPezd5OGv2mKjCjfJALRLm4wW50BjtnP-fAhdGIMO7vppiVm2RzTCsE98KHnV_9NSkSI2hBRpcBY--7-xi6gljHSSka5_9TAVaOip3MdaEtmQPDPzIyAbdQg/DSC_0580.jpg?psid=1&width=2558&height=3836'},
      { source: 'https://bn1ap000files.storage.live.com/y4mrMtEyYSjZ__qWkKnIyvGn1F3i5BOW_gGy8OiJTkDszgPAqisHK5zOfkr0AhuAXl19G08A5EBT4WnQvqZqyRpn1IdMX7zKanispeZ7sdsI2F8Yn5qJJ5VWqda9ozxbaMRPrFYZLDTm6VPX9uk167a_hZ4D3g5U6f7JQ0Rhwy8dMO9Zj5unOUJtT_p-l_KXVA1A9JcrW5sMSsbmOrqNPxZQw/DSC_0341.jpg?psid=1&width=2432&height=3648'},
      { source: 'https://snz04pap001files.storage.live.com/y4mWztkfVjBb7TiDM-sFsF_yUxkFA0GDZvMLCEIrm6bLpuVD0lNDSqJ5jfyj5zOowVVjWBOaLfkUbdtI30XlHp2nD0PnJcOam1J5-dy0DAV-5L1DjS2VdNVkOHU7DaO99GnuTm5d9QXQOHBjLf1Ffrq9Tv7ORRe6xnOFSYYvbyQcOsVxmNccc8kv3AnRaKptCwKZQ5hQFtmLzEoWzZ68atxCg/DSC_0601.jpg?psid=1&width=5754&height=3836'}
    ],
    showNumOfRemainingPhotos: true
  };

class GradPage extends Component{
    render(){
        return(
            <div className="collage-container"><ReactPhotoCollage {...setting} /></div>
            
        );
    }
}

export default GradPage;